import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { ellipse, square, triangle } from 'ionicons/icons';
import { CurrenciesStatus } from './pages/CurrenciesStatus/CurrenciesStatus';
import { AboutUs } from './pages/AboutUs/AboutUs';
import { NotFoundPage } from './pages/NotFoundPage/NotFoundPage';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonTabs>
        <IonRouterOutlet>
          <Route path="/currencies-status" component={CurrenciesStatus} exact={true} />
          <Route path="/about-us" component={AboutUs} exact={true}/>
          <Redirect exact from="/" to="/about-us" />
          <Route component={NotFoundPage} exact={true}/>
        </IonRouterOutlet>
        <IonTabBar slot="bottom">
          <IonTabButton tab="about-us" href="/about-us">
            <IonIcon icon={triangle} />
            <IonLabel>О нас</IonLabel>
          </IonTabButton>
          <IonTabButton tab="currencies-status" href="/currencies-status">
            <IonIcon icon={ellipse} />
            <IonLabel>Статус</IonLabel>
          </IonTabButton>
        </IonTabBar>
      </IonTabs>
    </IonReactRouter>
  </IonApp>
);

export default App;
