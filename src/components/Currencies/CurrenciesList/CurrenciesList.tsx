import React from 'react';
import { IonRow } from '@ionic/react';
import { CurrenciesListItem } from './CurrenciesListItem/CurrenciesListItem';
import {ICurrencies} from '../interfaces/ICurrencies';

export const CurrenciesList: React.FC<ICurrencies> = ({ currencies }) => {
    const currenciesContent = currencies.map((item) =>
        <CurrenciesListItem key={item.buy.toString()} currency={item}/>
    );

    return (
        <IonRow>
            {currenciesContent}
        </IonRow>
    );
};
