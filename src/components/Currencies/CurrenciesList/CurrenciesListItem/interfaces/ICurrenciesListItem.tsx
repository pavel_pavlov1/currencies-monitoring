import {ICurrenciesApiResponse} from '../../../interfaces/ICurrenciesApiResponse';

export interface ICurrenciesListItem {
    currency: ICurrenciesApiResponse;
    key: string;
}
