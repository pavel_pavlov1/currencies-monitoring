import React from 'react';
import {
    IonCol,
    IonCard,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
    IonCardContent,
} from '@ionic/react';
import { ICurrenciesListItem } from './interfaces/ICurrenciesListItem';
import './CurrenciesListItem.css';

export const CurrenciesListItem: React.FC<ICurrenciesListItem> = ({ currency }) => {
  return (
      <IonCol>
          <IonCard>
              <IonCardHeader>
                  <IonCardSubtitle>{currency.base_ccy}</IonCardSubtitle>
                  <IonCardTitle>{currency.ccy}</IonCardTitle>
              </IonCardHeader>

              <IonCardContent>
                  <div className="currencies-list-item-pricing">
                      <div>
                          <span className="ion-margin-end">Купить</span>
                          <span className="">Продать</span>
                      </div>

                      <div>
                          <span className="ion-margin-end">{currency.buy}</span>
                          <span className="">{currency.sale}</span>
                      </div>
                  </div>
              </IonCardContent>
          </IonCard>
      </IonCol>
  )
};
