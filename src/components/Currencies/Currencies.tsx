import React from 'react';
import { IonGrid, IonContent } from '@ionic/react';
import { CurrenciesList } from './CurrenciesList/CurrenciesList';
import {ICurrencies} from './interfaces/ICurrencies';

export const Currencies: React.FC<ICurrencies> = ({ currencies }) => {
    return (
        <IonContent>
            <IonGrid>
                <CurrenciesList currencies={currencies} />
            </IonGrid>
        </IonContent>
    );
};
