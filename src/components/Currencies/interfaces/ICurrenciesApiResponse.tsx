export interface ICurrenciesApiResponse {
    // currency which we want to see
    ccy: string;

    // our currency
    base_ccy: string;

    // price for buy
    buy: string;

    // price for sale
    sale: string;
}
