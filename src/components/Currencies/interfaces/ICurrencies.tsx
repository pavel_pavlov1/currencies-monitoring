import { ICurrenciesApiResponse } from './ICurrenciesApiResponse';

export interface ICurrencies {
    currencies: ICurrenciesApiResponse[];
}
