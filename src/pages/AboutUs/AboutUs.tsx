import React from 'react';
import {
    IonContent,
    IonPage,
    IonSlides,
    IonSlide,
    IonImg,
    IonTabButton,
    IonButton,
    IonTabBar,
} from '@ionic/react';

import './AboutUs.css';

export const AboutUs: React.FC = () => {
  const slideOpts = {
    initialSlide: 0,
    speed: 200,
  };

  return (
      <IonPage className="about-us">
          <IonContent>
              <IonSlides pager={true} options={slideOpts}>
                  <IonSlide>
                      <IonImg src="/assets/web_searching.svg" alt="image" />
                      <p>
                          Раньше мне приходилось искать текущий курс доллара, евро по отношению к гривне и посматривать чтобы он был выгодным для меня...
                      </p>
                  </IonSlide>

                  <IonSlide>
                      <IonImg src="/assets/idea.svg" alt="image" />
                      <p>
                          И тут пришла идея...
                      </p>
                  </IonSlide>

                  <IonSlide>
                      <IonImg src="/assets/undraw_solution_mindset_34bi.svg" alt="image" />
                      <p>
                          А сделаю-ка я сервис с помощью которого можно получать информацию о стоимости доллара, евро и т.д
                      </p>
                  </IonSlide>

                  <IonSlide>
                      <IonImg src="/assets/app_is_ready.svg" alt="image" />
                      <p>
                          И вот теперь вы можете попробовать мой сервис перейдя по этой ссылке
                          <IonTabBar slot="bottom">
                          <IonTabButton tab="currencies-status" href="/currencies-status">
                                  <IonButton color="tertiary">Нажми меня</IonButton>
                              </IonTabButton>
                          </IonTabBar>
                      </p>
                  </IonSlide>

              </IonSlides>
          </IonContent>
      </IonPage>
  );
};
