import React from 'react';
import { IonImg } from '@ionic/react';

import './NotFoundPage.css';

export const NotFoundPage: React.FC = () => {
  return (
    <div>
        <IonImg src="/assets/404.svg" alt="image"/>
    </div>
  );
};
