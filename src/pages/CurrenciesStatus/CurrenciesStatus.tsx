import React, {useEffect, useState} from 'react';
import {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    IonSpinner
} from '@ionic/react';
import { Currencies } from '../../components/Currencies/Currencies';
import './CurrenciesStatus.css';

export const CurrenciesStatus: React.FC = () => {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState([]);

    const getCurrenciesStatus = () => {
        return fetch('https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11');
    };

    useEffect(() => {
        // perform after first rendering
        setTimeout(() => {
            getCurrenciesStatus()
                .then((res) => res.json())
                .then((data) => {
                        setIsLoaded(true);
                        setItems(data);
                    },
                    // Note: it's important to handle errors here
                    // instead of a catch() block so that we don't swallow
                    // exceptions from actual bugs in components.
                    (error) => {
                        setIsLoaded(true);
                        setError(error);
                    })
        }, 0);

        // set interval for live currencies status update
        setInterval(() => {
            getCurrenciesStatus()
                .then((res) => res.json())
                .then((data) => {
                        setIsLoaded(true);
                        setItems(data);
                    },
                    // Note: it's important to handle errors here
                    // instead of a catch() block so that we don't swallow
                    // exceptions from actual bugs in components.
                    (error) => {
                        setIsLoaded(true);
                        setError(error);
                    })
        }, 5000);
    }, []);

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Мониторинг валюты</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <IonHeader collapse="condense">
                    <IonToolbar>
                        <IonTitle size="large">Курс валют</IonTitle>
                    </IonToolbar>
                </IonHeader>
                {
                    isLoaded
                    ? <Currencies currencies={items}/>
                    : <IonSpinner name="dots"/>
                }
            </IonContent>
        </IonPage>
    );
};
